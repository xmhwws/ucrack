package com.virjar.ucrack.hermes.weishi;

import com.virjar.hermes.hermesagent.hermes_api.ActionRequestHandler;
import com.virjar.hermes.hermesagent.hermes_api.WrapperAction;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeRequest;
import com.virjar.xposed_extention.ClassLoadMonitor;

import org.apache.commons.lang3.StringUtils;

import de.robv.android.xposed.XposedHelpers;

@WrapperAction("WSGetInRankingtStarList")
public class WSGetInRankingtStarListHandler implements ActionRequestHandler {
    @Override
    public Object handleRequest(InvokeRequest invokeRequest) {
        String personId = invokeRequest.getString("personId");
        if (StringUtils.isBlank(personId)) {
            personId = "1542254065354468";
        }
        String attach_info = invokeRequest.getString("attach_info");
        if (StringUtils.isBlank(attach_info)) {
            attach_info = "";
        }
        Object requestModel = XposedHelpers.newInstance(ClassLoadMonitor.tryLoadClass("NS_KING_INTERFACE.stWSGetInRankingtStarListReq"), attach_info, personId);
        return WeishiUtil.sendRequest(
                XposedHelpers.newInstance(ClassLoadMonitor.tryLoadClass("com.tencent.oscar.utils.network.l"), "WSGetInRankingtStarList", requestModel)
        );
    }
}
